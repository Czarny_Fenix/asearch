package com.figielski.grzegorz.asearch.Model;

/**
 * Created by Grzesiek on 19.11.2018.
 */

public class Transition {

    public Transition(float from, float to, float require) {
        this.from = from;
        this.to = to;
        this.require = require;
    }

    public float getFrom() {
        return from;
    }

    public float getTo() {
        return to;
    }

    public float getRequire() {
        return require;
    }

    float from;
    float to;
    float require;
}
