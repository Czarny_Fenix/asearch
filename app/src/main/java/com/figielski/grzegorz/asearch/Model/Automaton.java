package com.figielski.grzegorz.asearch.Model;

import static java.sql.Types.NULL;

/**
 * Created by Grzesiek on 19.11.2018.
 */

public class Automaton {

    public Automaton(double location_from, double location_to, double state, Transition[] transition) {
        if(state==NULL){
            this.state=location_from;
        } else {
            this.state = state;
        }
        this.location_from = location_from;
        this.location_to = location_to;
        this.transition = transition;
    }

    double location_from;
    double location_to;
    double state;
    Transition transition[];

}
